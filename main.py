import random
import pygame
import time

pygame.init()
screen = pygame.display.set_mode((800, 600))
image = pygame.image.load('arrow_0.png')
image2 = pygame.image.load('arrow_1.png')
image3 = pygame.image.load('yellow.png')
sound1 = pygame.mixer.Sound('b1.wav')
sound2 = pygame.mixer.Sound('b2.wav')
sound3 = pygame.mixer.Sound('b3.wav')
sound4 = pygame.mixer.Sound('b4.wav')
ooh = pygame.mixer.Sound('ooh.wav')
claping = pygame.mixer.Sound('clapping.wav')
sound_selection = {
    0: sound1,
    1: sound2,
    2: sound3,
    3: sound4
}
w, h = image.get_size()
get_random = True
start = 0
end = 0
flasher = 0

temp_val_1 = 0


def random_key():
    mylist = [273, 274, 276, 275]
    res = random.choice(mylist)
    return res


def screenlayout(arrow_direction=0, color=0):
    images = ['up', 'dowon', 'left', 'right']
    x = [309, 309, 109, 509]
    y = [56.5, 356.5, 206.5, 206.5]
    r = [0, 180, 90, 270]
    button = [273, 274, 276, 275, 0]
    screen.fill((255, 255, 255))
    ############ pictures place cordnates ###################
    # up = (x[0], y[0]) print(400 - (w / 2), 150 - (h / 2))
    # down = (x[1], y[1])print(400 - (w / 2), 450 - (h / 2))
    # left = (x[2], y[2])print(200 - (w / 2), 300 - (h / 2))
    # right = (x[3], y[3])print(600 - (w / 2), 300 - (h / 2))
    ##########################################################
    for coordinates in images:
        if images.index(coordinates) == button.index(arrow_direction):
            if color == 1:
                image_change = image2
                pygame.mixer.Sound.play(sound_selection[button.index(arrow_direction)])

            else:
                image_change = image3
        else:
            image_change = image
        image_rotate = pygame.transform.rotate(image_change, r[images.index(coordinates)])
        screen.blit(image_rotate, (x[images.index(coordinates)], y[images.index(coordinates)]))
    pygame.display.update()


selected_arrow = random_key()

running = True
state = 0
while running:
    start = time.thread_time()
    if get_random == True:
        state = random_key()
        get_random = False

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_q:
                running = False

            screenlayout(event.key, 1)
            if event.key == selected_arrow:
                pygame.mixer.Sound.play(claping)
            else:
                pygame.mixer.Sound.play(ooh)

        if event.type == pygame.KEYUP:
            if event.key == selected_arrow:
                selected_arrow = random_key()
    if event.type != pygame.KEYDOWN:
        if start - end >= 0.25:
            if flasher == 1:
                screenlayout(selected_arrow)
                flasher = 0
            elif flasher == 0:
                screenlayout()
                flasher = 1
            print(flasher)
            end = time.thread_time()